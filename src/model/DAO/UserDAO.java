package model.DAO;

import model.entity.Enum.Package_Type;
import model.entity.Insurance_Pakage;
import model.entity.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class UserDAO implements IDAO<User,String > {


    private final String sqlgetbyup = "select * from user where username=? and password =?";


    @Override
    public boolean insert(User user) {
        return false;
    }

    @Override
    public int executescalar() {
        return 0;
    }

    @Override
    public User updateById(User K) {
        return null;
    }

    @Override
    public boolean deleteById(User user) {
        return false;
    }

    @Override
    public List<User> getall() {
        return null;
    }

    @Override
    public User getById(String s) {
        return null;
    }
    public User getUserlogin(String user,String pass) {

        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        User user1 = null;
        try {

            ps = open.prepareStatement(sqlgetbyup);
            ps.setString(1,user);
            ps.setString(2,pass);
            rs = ps.executeQuery();

            while (rs.next()) {
                user1= new User();
                user1.setUsername(rs.getString("username"));
                user1.setPassword(rs.getString("password"));
                user1.setName(rs.getString("name"));
                user1.setRole(rs.getInt("role"));
                user1.setBiensoxe(rs.getInt("biensoxe"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return user1;
    }
}
