package model.DAO;

import java.util.List;

public interface IDAO <T, K>{
    boolean insert(T t);
    int executescalar();
    T updateById(T K);
    boolean deleteById(T t);
    List<T>getall();
    T getById(K k);
}
