package model.DAO;

import model.entity.Car;
import model.entity.Enum.Brand;
import model.entity.MediumCar;
import model.entity.ModermCar;
import model.entity.OldCar;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarDAO implements IDAO<Car,String> {
    private final String sqlinsert = "insert into car(carname,numberplate,yearmanufature,brand,haveisurance,actionduration,havepowersteering,havepositdevice) value(?,?,?,?,?,?,?,?)";
    private final String sqlgetall = "select * from car";
    private final String sqlgetcarnothaveinsurance = "select * from car where haveisurance=0";
//
//    private final String sqlgetcarnothaveinsurancelimit = "select * from car where haveisurance = 0 limit ?,?";
//    private final String sqlcountchni = "select Count(*) from car where haveisurance=0";
    private final String sqldem = "select count(*) FROM car";
    private final String sqlupdate = "update car set haveisurance = ? where numberplate = ?";
    private final String sqlgetbynumberplate = "select * from car where numberplate = ?";

    @Override
    public boolean insert(Car car) {

        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        try {
            String carname = "Car " + executescalar();
            ps = open.prepareStatement(sqlinsert);
            ps.setString(1, carname);
            ps.setString(2, car.getNumberPlate());
            ps.setInt(3, car.getYearManufacture());
            ps.setString(4, car.getBrand().toString());
            ps.setBoolean(5, car.isHaveIsurance());
            ps.setInt(6, 0);
            ps.setBoolean(7, false);
            ps.setBoolean(8, false);

            int row = ps.executeUpdate();
            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;
    }

    @Override
    public int executescalar() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = open.prepareStatement(sqldem);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return 0;
    }

//    public int getnumberchni() {
//        final Connection open = DBConection.open();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        try {
//            ps = open.prepareStatement(sqlcountchni);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                return rs.getInt(1);
//            }
//
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            DBConection.close(null, ps, open);
//        }
//        return 0;
//    }

    @Override
    public Car updateById(Car car) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        try {
            ps = open.prepareStatement(sqlupdate);
            ps.setBoolean(1, car.isHaveIsurance());
            ps.setString(2, car.getNumberPlate());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return null;
    }

    @Override
    public boolean deleteById(Car car) {
        return false;
    }

    @Override
    public List<Car> getall() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Car> list = null;
        try {
            ps = open.prepareStatement(sqlgetall);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                String carname = rs.getString("carname");
                String numberplate = rs.getString("numberplate");
                int namsanxuat = rs.getInt("yearmanufature");
                String loai = rs.getString("brand");
                boolean cobaohiem = rs.getBoolean("haveisurance");
                Brand brand = Brand.BMW;
                switch (loai) {
                    case "TOYOTA":
                        brand = Brand.TOYOTA;
                        break;
                    case "HUYNDAI":
                        brand = Brand.HUYNDAI;
                        break;
                    default:
                        brand = Brand.BMW;
                }
                if (namsanxuat <= 1995) {
                    OldCar car = new OldCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setActionDuration(rs.getInt("actionduration"));
                    list.add(car);
                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
                    MediumCar car = new MediumCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
                    list.add(car);
                } else if (namsanxuat >= 2005) {
                    ModermCar car = new ModermCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
                    list.add(car);
                }


            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }

    public List<Car> getallnothaveinsurance() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Car> list = null;
        try {
            ps = open.prepareStatement(sqlgetcarnothaveinsurance);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                String carname = rs.getString("carname");
                String numberplate = rs.getString("numberplate");
                int namsanxuat = rs.getInt("yearmanufature");
                String loai = rs.getString("brand");
                boolean cobaohiem = rs.getBoolean("haveisurance");
                Brand brand = Brand.BMW;
                switch (loai) {
                    case "TOYOTA":
                        brand = Brand.TOYOTA;
                        break;
                    case "HUYNDAI":
                        brand = Brand.HUYNDAI;
                        break;
                    default:
                        brand = Brand.BMW;
                }
                if (namsanxuat <= 1995) {
                    OldCar car = new OldCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setActionDuration(rs.getInt("actionduration"));
                    list.add(car);
                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
                    MediumCar car = new MediumCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
                    list.add(car);
                } else if (namsanxuat >= 2005) {
                    ModermCar car = new ModermCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
                    list.add(car);
                }


            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;

    }

//    public List<Car> getallnothaveinsurancelimit( int currentpage,int recordperpage) {
//        final Connection open = DBConection.open();
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//        List<Car> list = null;
//        try {
//            ps = open.prepareStatement(sqlgetcarnothaveinsurancelimit);
//            ps.setInt(1,currentpage);
//            ps.setInt(2,recordperpage);
//            rs = ps.executeQuery();
//            list = new ArrayList<>();
//            while (rs.next()) {
//                String carname = rs.getString("carname");
//                String numberplate = rs.getString("numberplate");
//                int namsanxuat = rs.getInt("yearmanufature");
//                String loai = rs.getString("brand");
//                boolean cobaohiem = rs.getBoolean("haveisurance");
//                Brand brand = Brand.BMW;
//                switch (loai) {
//                    case "TOYOTA":
//                        brand = Brand.TOYOTA;
//                        break;
//                    case "HUYNDAI":
//                        brand = Brand.HUYNDAI;
//                        break;
//                    default:
//                        brand = Brand.BMW;
//                }
//                if (namsanxuat <= 1995) {
//                    OldCar car = new OldCar();
//                    car.setCarname(carname);
//                    car.setNumberPlate(numberplate);
//                    car.setYearManufacture(namsanxuat);
//                    car.setBrand(brand);
//                    car.setHaveIsurance(cobaohiem);
//                    car.setActionDuration(rs.getInt("actionduration"));
//                    list.add(car);
//                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
//                    MediumCar car = new MediumCar();
//                    car.setCarname(carname);
//                    car.setNumberPlate(numberplate);
//                    car.setYearManufacture(namsanxuat);
//                    car.setBrand(brand);
//                    car.setHaveIsurance(cobaohiem);
//                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
//                    list.add(car);
//                } else if (namsanxuat >= 2005) {
//                    ModermCar car = new ModermCar();
//                    car.setCarname(carname);
//                    car.setNumberPlate(numberplate);
//                    car.setYearManufacture(namsanxuat);
//                    car.setBrand(brand);
//                    car.setHaveIsurance(cobaohiem);
//                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
//                    list.add(car);
//                }
//
//
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            DBConection.close(null, ps, open);
//        }
//        return list;
//
//    }


    @Override
    public Car getById(String s) {
        return null;
    }
    public Car getByNumberPlate(String s) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = open.prepareStatement(sqlgetbynumberplate);
            ps.setString(1, s);
            rs = ps.executeQuery();
            while (rs.next()) {
                String carname = rs.getString("carname");
                String numberplate = rs.getString("numberplate");
                int namsanxuat = rs.getInt("yearmanufature");
                String loai = rs.getString("brand");
                boolean cobaohiem = rs.getBoolean("haveisurance");
                Brand brand = Brand.BMW;
                switch (loai) {
                    case "TOYOTA":
                        brand = Brand.TOYOTA;
                        break;
                    case "HUYNDAI":
                        brand = Brand.HUYNDAI;
                        break;
                    default:
                        brand = Brand.BMW;
                }
                if (namsanxuat <= 1995) {
                    OldCar car = new OldCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setActionDuration(rs.getInt("actionduration"));
                   return car;
                } else if (namsanxuat >= 1996 && namsanxuat <= 2004) {
                    MediumCar car = new MediumCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavPowersteering(rs.getBoolean("havepowersteering"));
                    return car;
                } else if (namsanxuat >= 2005) {
                    ModermCar car = new ModermCar();
                    car.setCarname(carname);
                    car.setNumberPlate(numberplate);
                    car.setYearManufacture(namsanxuat);
                    car.setBrand(brand);
                    car.setHaveIsurance(cobaohiem);
                    car.setHavePositdevice(rs.getBoolean("havepositdevice"));
                    return car;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return null;
    }


}
