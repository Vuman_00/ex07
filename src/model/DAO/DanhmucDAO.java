package model.DAO;

import model.entity.*;
import model.entity.Enum.Brand;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DanhmucDAO implements IDAO<Danhmuc, Integer> {

    private final String sqlgetall = "select * from danhmuc";

    @Override
    public boolean insert(Danhmuc danhmuc) {
        return false;
    }

    @Override
    public int executescalar() {
        return 0;
    }

    @Override
    public Danhmuc updateById(Danhmuc K) {
        return null;
    }

    @Override
    public boolean deleteById(Danhmuc danhmuc) {
        return false;
    }

    @Override
    public List<Danhmuc> getall() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Danhmuc> list = null;
        try {
            ps = open.prepareStatement(sqlgetall);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                int iddanhmuc = rs.getInt("iddanhmuc");
                String noidung = rs.getString("noidung");
                Danhmuc danhmuc = new Danhmuc();
                danhmuc.setIddanhmuc(iddanhmuc);
                danhmuc.setTendanhmuc(noidung);
                list.add(danhmuc);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }

    @Override
    public Danhmuc getById(Integer integer) {
        return null;
    }
}
