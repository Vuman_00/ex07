package model.DAO;

import model.entity.Chitietbantin;
import model.entity.Danhmuc;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChitietbantinDAO implements IDAO<Chitietbantin, Integer> {
    private final String sqlinsert = "insert into chitietbantin(tieude,noidung,tomtat,solanxem,ngaydang,lkanh,iddanhmuc) value(?,?,?,?,?,?,?)";
    private final String sqlgetall = "select * from chitietbantin";
    private final String sqlgettop6hot = "select * from chitietbantin order by ngaydang desc limit 6";
    private final String sqlgetbyid = "select * from chitietbantin where idchitiet=?";
    private final String   sqlupdateslx = "update chitietbantin set solanxem = ? where idchitiet = ?";


    @Override
    public boolean insert(Chitietbantin chitietbantin) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        try {
            ps = open.prepareStatement(sqlinsert);
            ps.setString(1, chitietbantin.getTieude());
            ps.setString(2, chitietbantin.getNoidung());
            ps.setString(3, chitietbantin.getTomtat());
            ps.setInt(4,0);
            ps.setDate(5,  java.sql.Date.valueOf(java.time.LocalDate.now()));
            ps.setString(6,chitietbantin.getLkanh());
            ps.setInt(7,chitietbantin.getIddanhmuc());

            int row = ps.executeUpdate();
            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;
    }

    @Override
    public int executescalar() {
        return 0;
    }

    @Override
    public Chitietbantin updateById(Chitietbantin K) {
        return null;
    }
    public boolean updateslxem(Chitietbantin bantin) {

        final Connection open = DBConection.open();
        PreparedStatement ps = null;

        try {
            ps = open.prepareStatement(sqlupdateslx);
            ps.setInt(1,bantin.getSolanxem()+1);
            ps.setInt(2,bantin.getIdchitiet());
            int ok = ps.executeUpdate();
            if (ok>0)return true;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }

        return false;
    }

    @Override
    public boolean deleteById(Chitietbantin chitietbantin) {
        return false;
    }

    @Override
    public List<Chitietbantin> getall() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Chitietbantin> list = null;
        try {
            ps = open.prepareStatement(sqlgettop6hot);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                Chitietbantin chitietbantin = new Chitietbantin();
                chitietbantin.setIdchitiet(rs.getInt("idchitiet"));
                chitietbantin.setTieude(rs.getString("tieude"));
                chitietbantin.setNoidung(rs.getString("noidung"));
                chitietbantin.setTomtat(rs.getString("tomtat"));
                chitietbantin.setSolanxem(rs.getInt("solanxem"));
                chitietbantin.setNgaydang(rs.getDate("ngaydang"));
                chitietbantin.setIddanhmuc(rs.getInt("iddanhmuc"));
                chitietbantin.setLkanh(rs.getString("lkanh"));
                list.add(chitietbantin);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }

    @Override
    public Chitietbantin getById(Integer integer) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = open.prepareStatement(sqlgetbyid);
            ps.setInt(1, integer);
            rs = ps.executeQuery();
            while (rs.next()) {
                Chitietbantin chitietbantin = new Chitietbantin();
                chitietbantin.setIdchitiet(rs.getInt("idchitiet"));
                chitietbantin.setTieude(rs.getString("tieude"));
                chitietbantin.setNoidung(rs.getString("noidung"));
                chitietbantin.setTomtat(rs.getString("tomtat"));
                chitietbantin.setSolanxem(rs.getInt("solanxem"));
                chitietbantin.setNgaydang(rs.getDate("ngaydang"));
                chitietbantin.setIddanhmuc(rs.getInt("iddanhmuc"));
                chitietbantin.setLkanh(rs.getString("lkanh"));
                return chitietbantin;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return null;
    }
}
