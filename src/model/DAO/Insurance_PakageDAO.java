package model.DAO;
import model.entity.*;
import model.entity.Enum.Package_Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class Insurance_PakageDAO implements IDAO<Insurance_Pakage,Integer> {
    private final String sqlinsert = "insert into insurance_pakage(tengoibaohiem,biensoxe,trangthai,loaibaohiem) value(?,?,?,?)";
    private final String sqlupdate = "update insurance_pakage set trangthai = ?,biensoxe=? where tengoibaohiem = ?";
    private final String sqlgetall = "select * from insurance_pakage";
    private final String sqldem = "select count(*) FROM insurance_pakage";
    private final String sqlgetbyname = "select * from insurance_pakage where tengoibaohiem=?";
    private final String sqlgetallnotbuy = "select * from insurance_pakage where trangthai=0";

    @Override
    public boolean insert(Insurance_Pakage pakage) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        boolean thanhcong = false;
        try {
            String name = "Insurance_Package_" + executescalar();
            ps = open.prepareStatement(sqlinsert);
            ps.setString(1, name);
            ps.setString(2, pakage.getBiensoxe());
            ps.setBoolean(3, pakage.isTrangthaibaohiem());
            ps.setString(4, pakage.getLoaibaohien().toString());
            int row = ps.executeUpdate();
            if (row > 0) {
                thanhcong = true;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return thanhcong;
    }

    @Override
    public int executescalar() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = open.prepareStatement(sqldem);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return 0;
    }

    @Override
    public Insurance_Pakage updateById(Insurance_Pakage pakage) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        try {
            ps = open.prepareStatement(sqlupdate);
            ps.setBoolean(1, pakage.isTrangthaibaohiem());
            ps.setString(2, pakage.getBiensoxe());
            ps.setString(3, pakage.getTengoibaohiem());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return null;
    }

    @Override
    public boolean deleteById(Insurance_Pakage pakage) {
        return false;
    }

    @Override
    public List<Insurance_Pakage> getall() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Insurance_Pakage> list = null;
        try {
            ps = open.prepareStatement(sqlgetall);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                String loai = rs.getString("loaibaohiem");
                Package_Type type = Package_Type.C;
                switch (loai) {
                    case "A":
                        type = Package_Type.A;
                        break;
                    case "B":
                        type = Package_Type.B;
                        break;
                    default:
                        type = Package_Type.C;
                }
                Insurance_Pakage pakage = new Insurance_Pakage();
                pakage.setTengoibaohiem(rs.getString("tengoibaohiem"));
                pakage.setBiensoxe(rs.getString("biensoxe"));
                pakage.setTrangthaibaohiem(rs.getBoolean("trangthai"));
                pakage.setLoaibaohien(type);
                list.add(pakage);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }

    @Override
    public Insurance_Pakage getById(Integer integer) {
        return null;
    }


    public  Insurance_Pakage getbyname(String tengoibh) {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {

            ps = open.prepareStatement(sqlgetbyname);
            ps.setString(1,tengoibh);
            rs = ps.executeQuery();

            while (rs.next()) {
                String loai = rs.getString("loaibaohiem");
                Package_Type type = Package_Type.C;
                switch (loai) {
                    case "A":
                        type = Package_Type.A;
                        break;
                    case "B":
                        type = Package_Type.B;
                        break;
                    default:
                        type = Package_Type.C;
                }
                Insurance_Pakage pakage = new Insurance_Pakage();
                pakage.setTengoibaohiem(rs.getString("tengoibaohiem"));
                pakage.setBiensoxe(rs.getString("biensoxe"));
                pakage.setTrangthaibaohiem(rs.getBoolean("trangthai"));
                pakage.setLoaibaohien(type);
                return pakage;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return null;
    }

    public List<Insurance_Pakage> getallnotbuy() {
        final Connection open = DBConection.open();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Insurance_Pakage> list = null;
        try {
            ps = open.prepareStatement(sqlgetallnotbuy);
            rs = ps.executeQuery();
            list = new ArrayList<>();
            while (rs.next()) {
                String loai = rs.getString("loaibaohiem");
                Package_Type type = Package_Type.C;
                switch (loai) {
                    case "A":
                        type = Package_Type.A;
                        break;
                    case "B":
                        type = Package_Type.B;
                        break;
                    default:
                        type = Package_Type.C;
                }
                Insurance_Pakage pakage = new Insurance_Pakage();
                pakage.setTengoibaohiem(rs.getString("tengoibaohiem"));
                pakage.setBiensoxe(rs.getString("biensoxe"));
                pakage.setTrangthaibaohiem(rs.getBoolean("trangthai"));
                pakage.setLoaibaohien(type);
                list.add(pakage);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBConection.close(null, ps, open);
        }
        return list;
    }
}
