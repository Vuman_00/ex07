package model.entity;

public class ModermCar extends Car {
private boolean havePositdevice;

    public boolean isHavePositdevice() {
        return havePositdevice;
    }

    public void setHavePositdevice(boolean havePositdevice) {
        this.havePositdevice = havePositdevice;
    }

    @Override
    public String toString() {
        return super.toString()+", havePositdevice=" + havePositdevice + '}'+"Xe mới";
    }


}
