package model.entity;

import model.entity.Enum.Package_Type;

public class Insurance_Pakage {
    private String tengoibaohiem;
    private String biensoxe = null;//biển số
    private  boolean trangthaibaohiem=false;//đã mua hoặc chưa mua
    private Package_Type loaibaohien;//loại nào

    public Insurance_Pakage() {
    }

    public Insurance_Pakage(String tengoibaohiem, String biensoxe, boolean trangthaibaohiem, Package_Type loaibaohien) {
        this.tengoibaohiem = tengoibaohiem;
        this.biensoxe = biensoxe;
        this.trangthaibaohiem = trangthaibaohiem;
        this.loaibaohien = loaibaohien;
    }

    public String getTengoibaohiem() {
        return tengoibaohiem;
    }

    public void setTengoibaohiem(String tengoibaohiem) {
        this.tengoibaohiem = tengoibaohiem;
    }

    public String getBiensoxe() {
        return biensoxe;
    }

    public void setBiensoxe(String biensoxe) {
        this.biensoxe = biensoxe;
    }

    public boolean isTrangthaibaohiem() {
        return trangthaibaohiem;
    }

    public void setTrangthaibaohiem(boolean trangthaibaohiem) {
        this.trangthaibaohiem = trangthaibaohiem;
    }

    public Package_Type getLoaibaohien() {
        return loaibaohien;
    }

    public void setLoaibaohien(Package_Type loaibaohien) {
        this.loaibaohien = loaibaohien;
    }


    @Override
    public String toString() {
        return "Insurance_Pakage{" +
                "tengoibaohiem='" + tengoibaohiem + '\'' +
                ", biensoxe='" + biensoxe + '\'' +
                ", trangthaibaohiem=" + trangthaibaohiem +
                ", loaibaohien=" + loaibaohien +
                '}';
    }
}
