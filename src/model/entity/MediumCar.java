package model.entity;

public class MediumCar extends Car {

    private  boolean havPowersteering;

    public boolean isHavPowersteering() {
        return havPowersteering;
    }

    public void setHavPowersteering(boolean havPowersteering) {
        this.havPowersteering = havPowersteering;
    }
    @Override
    public String toString() {
        return super.toString()+ ", havePositdevice=" + havPowersteering + '}'+"Xe bình thường";
    }


}
