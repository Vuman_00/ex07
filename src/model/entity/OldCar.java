package model.entity;

public class OldCar extends Car {
    private int actionDuration;

    public int getActionDuration() {
        return actionDuration;
    }

    public void setActionDuration(int actionDuration) {
        this.actionDuration = actionDuration;
    }

    @Override
    public String toString() {
        return super.toString()+", actionDuration=" + actionDuration + '}'+"Xe cũ";
    }

}
