package controller;

import com.google.gson.Gson;
import model.DAO.CarDAO;
import model.entity.Car;
import model.entity.MediumCar;
import model.entity.ModermCar;
import model.entity.OldCar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

@WebServlet(name = "CarInfomationsServlet", urlPatterns = {"/CarInfomationsServlet"})
public class CarInfomationsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if((Boolean) session.getAttribute("islogin")) {
            //Đăng nhập mới lấy được dữ liệu

        CarDAO carDAO= new CarDAO();
        String bienso = request.getParameter("bienso");
        Car car = carDAO.getByNumberPlate(bienso);
        String loaixe="";
        String thuoctinhphu="";
        String giatriphu="";

        if(car instanceof ModermCar){
            loaixe="Moderm Car";
            thuoctinhphu="Have positioning device";
            ModermCar modermCar = (ModermCar) car;
            giatriphu= String.valueOf(modermCar.isHavePositdevice());
        }
        else if(car instanceof MediumCar){
            loaixe="Medium Car";
            thuoctinhphu="Have power steering";
            MediumCar mediumCar = (MediumCar) car;
            giatriphu= String.valueOf(mediumCar.isHavPowersteering());
        }
        else {
            loaixe="Old Car";
            thuoctinhphu="Action Duration";
           OldCar oldCar = (OldCar) car;
            giatriphu= String.valueOf(oldCar.getActionDuration());
        }
        Map<String, String> carInfos = new LinkedHashMap<>();
        carInfos.put("loaixe",loaixe);
        carInfos.put("thuoctinhphu", thuoctinhphu);
        carInfos.put("giatriphu", giatriphu);
        carInfos.put("tenxe",car.getCarname());
        carInfos.put("bienso",car.getNumberPlate());
        carInfos.put("namsx", String.valueOf(car.getYearManufacture()));
        carInfos.put("hangxe", String.valueOf(car.getBrand()));
        carInfos.put("baohiem", String.valueOf(car.isHaveIsurance()));
        String json = new Gson().toJson(carInfos);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    }
        else {
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write("Bạn cần đăng nhập!");
        }
    }
}
