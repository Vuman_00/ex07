package controller;

import com.google.gson.Gson;
import model.DAO.CarDAO;
import model.DAO.Insurance_PakageDAO;
import model.entity.Car;
import model.entity.Enum.Package_Type;
import model.entity.Insurance_Pakage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


@WebServlet(name = "BuyingDetailServlet", urlPatterns = {"/BuyingDetailServlet"})
public class BuyingDetailServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String bienso = request.getParameter("bienso");
        String tengoibh= request.getParameter("tengoibaohiem");
        Car car = new CarDAO().getByNumberPlate(bienso);
        Insurance_Pakage pakage =new Insurance_PakageDAO().getbyname(tengoibh);
        Map<String, String> traketqua = acceptBuyingPakage(car,pakage);
        if(traketqua.isEmpty()){ traketqua.put("false","Unsuccessful Buying");}
            String json = new Gson().toJson(traketqua);
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);

    }
    public Map<String,String> acceptBuyingPakage(Car car, Insurance_Pakage pakage){

        Map<String, String> ketqua = new LinkedHashMap<String, String>();
        if(car==null||pakage==null){return ketqua;}
        boolean goiphuhop = isvalid_Pakage(car,pakage.getLoaibaohien());
        if(car.isHaveIsurance())
        {
            ketqua.put("false","Unavailable Buying!");
            return ketqua;
        }
        else
        if (!goiphuhop) {

            ketqua.put("false","Invalid Package!");
            return ketqua;
        }
        else
        if (!car.isHaveIsurance()&& goiphuhop)
        {
            car.setHaveIsurance(true);
            pakage.setTrangthaibaohiem(true);
            pakage.setBiensoxe(car.getNumberPlate());
            new CarDAO().updateById(car);
            new Insurance_PakageDAO().updateById(pakage);

            ketqua.put("success","Successful Buying");
            return ketqua;

        }else {
//            ketqua.put("false","Unsuccessful Buying");
//            return ketqua;
            return ketqua;
        }

    }

    public boolean isvalid_Pakage(Car car, Package_Type package_type){

            if (car.getYearManufacture() >= 2005 && package_type != Package_Type.A)
                return false;
            if (car.getYearManufacture() >= 1996 && car.getYearManufacture() <= 2004 && package_type != Package_Type.B)
                return false;
            if (car.getYearManufacture() <= 1995 && package_type != Package_Type.C)
                return false;
        return true;
    }
}
