package controller;

import model.DAO.ChitietbantinDAO;
import model.entity.Chitietbantin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChitietbantinServlet", urlPatterns = {"/ChitietbantinServlet"})
public class ChitietbantinServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ChitietbantinDAO ct= new ChitietbantinDAO();
        int idchitiet = Integer.parseInt(request.getParameter("idchitiet"));
        Chitietbantin chitietbantin = ct.getById(idchitiet);
        if (ct.updateslxem(chitietbantin)){
        request.setAttribute("chitietbantin", chitietbantin);
        request.getRequestDispatcher("Chitietbantin.jsp").forward(request, response);}
    }
}
