package controller;

import model.DAO.Insurance_PakageDAO;
import model.entity.Insurance_Pakage;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShowInsurancePakageServlet", urlPatterns = {"/ShowInsurancePakageServlet"})
public class ShowInsurancePakageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        if(!(Boolean) session.getAttribute("islogin")) {
            response.sendRedirect(request.getContextPath()+"/logout.jsp");
        }
        else {
            Insurance_PakageDAO insurance_pakageDAO = new Insurance_PakageDAO();
            ArrayList<Insurance_Pakage> getall = (ArrayList<Insurance_Pakage>) insurance_pakageDAO.getall();

            request.setAttribute("listInsurance", getall);
            request.getRequestDispatcher("ShowInsurancePakage.jsp").forward(request, response);
        }


    }


}
