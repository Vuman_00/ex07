package controller;

import model.DAO.CarDAO;
import model.DAO.ChitietbantinDAO;
import model.entity.Car;
import model.entity.Chitietbantin;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "IndexServlet", urlPatterns = {"/IndexServlet"})
public class IndexServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        try{
            ChitietbantinDAO chitietbantinDAO = new ChitietbantinDAO();
        ArrayList<Chitietbantin> listchitiet = (ArrayList<Chitietbantin>) chitietbantinDAO.getall();

        CarDAO carDAO = new CarDAO();
        ArrayList<Car> carArrayList = (ArrayList<Car>) carDAO.getallnothaveinsurance();
//
//            int currentPage = Integer.valueOf(request.getParameter("currentPage"));
//            int recordsPerPage = Integer.valueOf(request.getParameter("recordsPerPage"));
//
//            int rows = carArrayList.size();
//            int nOfPages = rows / recordsPerPage;
//
//            if (nOfPages % recordsPerPage > 0) {
//                nOfPages++;
//            }
//            request.setAttribute("noOfPages", nOfPages);
//            request.setAttribute("currentPage", currentPage);
//            request.setAttribute("recordsPerPage", recordsPerPage);

        request.setAttribute("listcarnothaveinsurance", carArrayList);
        request.setAttribute("listchitietbantin", listchitiet);
        request.getRequestDispatcher("index.jsp").forward(request, response);
//        }catch (Exception ex){
//            System.out.println("Loi");
//        }
    }
}

