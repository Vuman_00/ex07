package controller;

import model.DAO.DanhmucDAO;
import model.entity.Danhmuc;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "HomeServlet", urlPatterns = {"/HomeServlet"})
public class HomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DanhmucDAO danhmucDAO = new DanhmucDAO();
        ArrayList<Danhmuc> danhmucs = (ArrayList<Danhmuc>) danhmucDAO.getall();
        request.setAttribute("listdanhmuc", danhmucs);
        request.getRequestDispatcher("header.jsp").include(request, response);
    }

}
