package controller;

import model.DAO.CarDAO;
import model.DAO.Insurance_PakageDAO;
import model.entity.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "BuyingInsurancePakageServlet",urlPatterns = {"/BuyingInsurancePakageServlet"})
public class BuyingInsurancePakageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String bienso= request.getParameter("bienso");
        Car car = new CarDAO().getByNumberPlate(bienso);
        String loaixe="";
        String thuoctinhphu="";
        String giatriphu="";

        Insurance_PakageDAO insurance_pakageDAO = new Insurance_PakageDAO();
        ArrayList<Insurance_Pakage> getall = (ArrayList<Insurance_Pakage>) insurance_pakageDAO.getallnotbuy();

        request.setAttribute("listInsurance",getall);

        if(car instanceof ModermCar){
            loaixe="Moderm Car";
            thuoctinhphu="Have positioning device";
            ModermCar modermCar = (ModermCar) car;
            giatriphu= String.valueOf(modermCar.isHavePositdevice());
        }
        else if(car instanceof MediumCar){
            loaixe="Medium Car";
            thuoctinhphu="Have power steering";
            MediumCar mediumCar = (MediumCar) car;
            giatriphu= String.valueOf(mediumCar.isHavPowersteering());
        }
        else {
            loaixe="Old Car";
            thuoctinhphu="Action Duration";
            OldCar oldCar = (OldCar) car;
            giatriphu= String.valueOf(oldCar.getActionDuration());
        }


        request.setAttribute("car",car);
        request.setAttribute("loaixe",loaixe);
        request.setAttribute("thuoctinhphu",thuoctinhphu);
        request.setAttribute("giatriphu",giatriphu);
     request.getRequestDispatcher("BuyingInsurancePakage.jsp").forward(request,response);
    }
}
