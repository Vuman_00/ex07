package controller;

import model.DAO.CarDAO;
import model.entity.Car;
import model.entity.MediumCar;
import model.entity.ModermCar;
import model.entity.OldCar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "ShowAllCarServlet", urlPatterns = {"/ShowAllCarServlet"})
public class ShowAllCarServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        if(!(Boolean) session.getAttribute("islogin")) {
            response.sendRedirect(request.getContextPath()+"/logout.jsp");
        }
        else {
            CarDAO carDAO = new CarDAO();
            ArrayList<Car> carArrayList = (ArrayList<Car>) carDAO.getall();
            String cartype = "All Car";

            int loai = Integer.parseInt(request.getParameter("lx"));
            switch (loai) {
                case 1:
                    ArrayList<OldCar> oldCars = new ArrayList<>();
                    for (Car oto : carArrayList) {
                        if (oto instanceof OldCar)
                            oldCars.add((OldCar) oto);

                    }
                    cartype = "Old Car";
                    request.setAttribute("listCar", oldCars);
                    break;
                case 2:
                    ArrayList<MediumCar> mediumCars = new ArrayList<>();
                    for (Car oto : carArrayList) {
                        if (oto instanceof MediumCar)
                            mediumCars.add((MediumCar) oto);
                    }
                    request.setAttribute("listCar", mediumCars);
                    cartype = "Medium Car";
                    break;
                case 3:
                    ArrayList<ModermCar> modermCars = new ArrayList<>();
                    for (Car oto : carArrayList) {
                        if (oto instanceof ModermCar)
                            modermCars.add((ModermCar) oto);
                    }
                    request.setAttribute("listCar", modermCars);
                    cartype = "Moderm Car";
                    break;
                default:
                    request.setAttribute("listCar", carArrayList);
                    break;
            }
            request.setAttribute("cartype", cartype);
            request.getRequestDispatcher("ShowAllCar.jsp").forward(request, response);
        }
    }
}
