package controller;

import com.google.gson.Gson;
import model.DAO.ChitietbantinDAO;
import model.DAO.DanhmucDAO;
import model.entity.Chitietbantin;
import model.entity.Danhmuc;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeParseException;
import java.util.*;

@WebServlet(name = "ThembantinServlet", urlPatterns = {"/ThembantinServlet"})
public class ThembantinServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ketqua ="false";
        try{
            String tieude= request.getParameter("tieude");
            String noidung= request.getParameter("noidung");
            String tomtat= request.getParameter("tomtat");
            String linkanh= request.getParameter("linkanh");
            int madanhmuc = Integer.parseInt(request.getParameter("danhmuc"));


            Chitietbantin chitietbantin = new Chitietbantin();
            chitietbantin.setTieude(tieude);
            chitietbantin.setNoidung(noidung);;
            chitietbantin.setTomtat(tomtat);
            chitietbantin.setLkanh(linkanh);
            chitietbantin.setIddanhmuc(madanhmuc);


            ChitietbantinDAO chitietbantinDAO = new ChitietbantinDAO();
            if (  chitietbantinDAO.insert(chitietbantin))
            {
                ketqua="true";
                System.out.println("Them ban tin thanh cong");
            }

        }catch (Exception ex){
            // do nothing
            System.out.println("Them ban tin loi");

        }finally {
            response.setContentType("text/plain");
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(ketqua);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DanhmucDAO danhmucDAO = new DanhmucDAO();
        ArrayList<Danhmuc> listdanhmuc = (ArrayList<Danhmuc>) danhmucDAO.getall();
        request.setAttribute("listdanhmuc",listdanhmuc);

        request.getRequestDispatcher("Thembantin.jsp").forward(request,response);

    }
}
