<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 7/17/2019
  Time: 11:05 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Mua bảo hiểm cho xe</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/buyingcar.js"></script>

</head>
<body>
<div>
    <jsp:include page="HomeServlet"></jsp:include>
</div>
<div class="container">
    <div class="thongtinxe">
    <h1>Thông tin xe</h1>
    <b> <p>Loại xe:  <span class="badge badge-primary" id="loaixe">${loaixe}</span></p></b>

        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <p>Tên xe:  <span class="badge badge-danger" id="tenxe">${car.getCarname()}</span></p>
                <p> Biển số :  <span class="badge badge-danger" id="bienso">${car.getNumberPlate()}</span> </p>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <p>Năm sản xuất : <span class="badge badge-danger" id="namsx">${car.getYearManufacture()}</span></p>
                <p> Hãng xe : <span class="badge badge-danger" id="hangxe">${car.getBrand()}</span></p>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <p> Trạng thái bảo hiểm :   <span class="badge badge-danger" id="baohiem">${car.isHaveIsurance()}</span></p>
                <p><span id="thuoctinhphu">${thuoctinhphu}</span> : <span class="badge badge-danger" id="giatriphu">${giatriphu}</span> </p>
            </div>
        </div>
        </div>
<h1>Chọn gói bảo hiểm muốn mua</h1>

        <table class="table table-hover" id="tbgoibh">
            <thead>
            <th>Tên gói bảo hiểm</th>
            <th>Biển số xe </th>
            <th>Trạng thái</th>
            <th>Loại bảo hiểm</th>
            <th>Hành động</th>

            </thead>
            <tbody>
            <c:forEach items="${listInsurance}" var="c">
                <tr class="cltr">
                    <td>${c.tengoibaohiem}</td>
                    <td>
                        <c:if test="${c.biensoxe==null}">
                            <span class="badge badge-danger">Chưa mua</span>
                        </c:if>
                        <c:if test="${c.biensoxe!=null}">
                            ${c.biensoxe}
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${c.trangthaibaohiem==false}">
                            <span class="badge badge-danger">Chưa mua</span>
                        </c:if>
                        <c:if test="${c.trangthaibaohiem==true}">
                            <span class="badge badge-primary">Đã mua</span>
                        </c:if>
                    </td>
                    <td>${c.loaibaohien}</td>
                    <td>
                        <a href="#ketquamua" class="muabh" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Mua">&#xe854;</i></a> </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
</div>


	<div id="ketquamua" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">
						<h2 class="modal-title">Xác nhận mua gói bảo hiểm</h2>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">

						<p class="text-warning" id="canhbao">Bạn có chắc muốn mua gói  bảo hiểm này<br>
                            Hành động này sẽ không thể hoàn tác.</p>
                        <div id="ketqua"></div>
                        <span id="thongbaokq" style="display: none" class="alert alert-warning">Loading...</span>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="button" id="muangay" class="btn btn-primary" value="Mua ngay">
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
