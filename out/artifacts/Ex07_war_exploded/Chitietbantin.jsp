<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 22/07/2019
  Time: 1:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chi tiết bản tin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="styles/chitietbantin.css">
</head>
<body>
<div><jsp:include page="HomeServlet"></jsp:include></div>
<div class="container">
    <button class="btn btn-warning">Lượt xem
        <span class="badge badge-danger"> <p>${chitietbantin.getSolanxem()}</p></span>
    </button><br><br>
    <strong>Ngày đăng:</strong><span class="badge badge-primary"><p>${chitietbantin.getNgaydang()}</p>
</span>
    <h1>${chitietbantin.getTieude()}</h1>
    <p>${chitietbantin.getNoidung()}</p>
</div>

</body>
</html>
