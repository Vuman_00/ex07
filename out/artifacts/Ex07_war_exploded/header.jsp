<%@ page import="model.entity.User" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 22/07/2019
  Time: 5:37 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

</head>
<body>
<div class="container">
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.jsp">VUMANCAR</a>
            </div>
            <ul class="nav navbar-nav">

                <c:forEach items="${listdanhmuc}" var="danhmuc">
                    <li><a href="#?iddanhmuc=${danhmuc.getIddanhmuc()}">${danhmuc.getTendanhmuc()}</a></li>
                </c:forEach>


                <li><a href="#">All Car</a></li>
                <li><a href="ThembantinServlet">Contact</a></li>

                <%
                    if ((Boolean) session.getAttribute("islogin")) {
                %>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Car Manager
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="ShowAllCarServlet?lx=0">Show All Car</a></li>
                        <li><a href="ShowInsurancePakageServlet">Show InsurancePakage</a></li>
                    </ul>
                </li>
                <%
                    }
                %>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <%
                    if ((Boolean) session.getAttribute("islogin")) {
                        User user = (User) session.getAttribute("user");
                %>
                <li><a href="#"><span class="glyphicon glyphicon-user"></span><%=user.getName()%></a></li>
                <li><a href="LogoutServlet"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                <%
                } else {
                %>
                <li><a href="login.jsp"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                <%}%>
            </ul>
        </div>
    </nav>
</div>


</body>
</html>
