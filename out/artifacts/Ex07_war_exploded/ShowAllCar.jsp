<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 7/11/2019
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Show all car</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/main.js"></script>

</head>
<body>
<div>
    <jsp:include page="HomeServlet"></jsp:include>
</div>

<div class="container">
    <h1>Car Manage</h1>
    <div class="row">
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">${cartype}
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a href="ShowAllCarServlet?lx=0">All Car</a></li>
                    <li><a href="ShowAllCarServlet?lx=1">Old Car</a></li>
                    <li><a href="ShowAllCarServlet?lx=2">Medium Car</a></li>
                    <li><a href="ShowAllCarServlet?lx=3">Modern Car</a></li>
                </ul>
            </div>
    </div>
        <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <input class="form-control" id="myInput" type="text" placeholder="Search..">
        </div>

    </div>
<table class="table table-hover" id="tbCar">
    <thead>
        <th>Car Name</th>
        <th>Number Plate</th>
        <th>Year Manufature</th>
        <th>Brand</th>
        <th>Have Insurant</th>
        <th>Actions</th>
    </thead>

    <tbody>
    <c:forEach items="${listCar}" var="c">
        <tr class="cltr">
            <td>${c.carname}</td>
            <td>${c.numberPlate}</td>
            <td>${c.yearManufacture}</td>
            <td>${c.brand}</td>
            <td>
                    ${c.haveIsurance}</td>
            <td>
                <a href="#xemxe" class="xem" data-toggle="modal"> <i class="material-icons"  data-toggle="tooltip" title="Xem">&#xe8f4;</i></a>
                <a href="#" class="sua" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Sửa">&#xE254;</i></a>
                <a href="#" class="xoa" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Xóa">&#xE872;</i></a>
                <a href="#" class="muabh" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Mua bảo hiểm">&#xe854;</i></a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</div>

<!-- Xem xe HTML -->
<div id="xemxe" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="" id="form-chitietbv">
                <div class="modal-header">
                    <h2 class="modal-title">Car Infomations</h2>
                    <button type="button" class="close" data-dismiss="modal" >&times;</button>
                </div>
                <div class="modal-body">
                    <h3>Chi tiết xe</h3>
                    <b> <p>Loại xe:  <span class="badge badge-primary" id="loaixe"></span></p></b>
                    <div class="modal-md" id="somediv">
        <div class="row">
               <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                     <p>Tên xe:  <span class="badge badge-danger" id="tenxe"></span></p>
                     <p> Biển số :  <span class="badge badge-danger" id="bienso"></span> </p>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <p>Năm sản xuất : <span class="badge badge-danger" id="namsx"></span></p>
                      <p> Hãng xe : <span class="badge badge-danger" id="hangxe"></span></p>
              </div>
              <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                      <p> Trạng thái bảo hiểm :   <span class="badge badge-danger" id="baohiem"></span></p>
                      <p><span id="thuoctinhphu"></span> : <span class="badge badge-danger" id="giatriphu"></span> </p>
               </div>
              </div>
        </div>
                 </div>
                <div class="modal-footer">
                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
