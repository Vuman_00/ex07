$(document).ready(function() {
    // Activate tooltip
    $('[data-toggle="tooltip"]').tooltip();

    $("#tbCar").on('click', '.xoa', function () {

        if(confirm("Delete this car ?")){
            $(this).closest('tr').remove();
        }
        else{
            return false;
        }
    });
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#tbCar tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    $("#tbCar").on('click', '.xem', function () {
        var $bienso = $('td:eq(1)', $(this).parent().parent()).text();
        $("p#tesss").text($bienso);
        $.get("CarInfomationsServlet",{bienso:$bienso}, function(responseJson) {
            $.each(responseJson, function(key,value) {    // Iterate over the JSON array.
                $("span#"+key).val(key).text(value);// Create HTML <td> element, set its text content with price of currently iterated product and append it to the <tr>.
            });
        });
    });

    $("#tbCar").on('click', '.muabh', function () {
        var $bienso = $('td:eq(1)', $(this).parent().parent()).text();
        window.location.href = 'BuyingInsurancePakageServlet?bienso='+ $bienso;

    });

});
