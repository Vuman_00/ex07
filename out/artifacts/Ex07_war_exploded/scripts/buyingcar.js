$(document).ready(function() {

    $('[data-toggle="tooltip"]').tooltip();

    $("#tbgoibh").on('click', '.muabh', function () {
        $("p#canhbao").show();
        $( "span#thongbaokq" ).hide();
        var $bienso = $("span#bienso").text();
        var $tengoibh = $('td:eq(0)', $(this).parent().parent()).text();
        $("#muangay").click(function() {

            $.get("BuyingDetailServlet",{bienso:$bienso,tengoibaohiem:$tengoibh}, function(responseJson) {
                $.each(responseJson, function(key,value) {
                    $("span#thongbaokq").val(key).text(value);
                    if (value=="Successful Buying"){
                        $("#tbgoibh").hide();
                    }

                });
            });
            $("p#canhbao").fadeOut("slow");
            $( "span#thongbaokq" ).fadeIn( "slow" );


        });
    });


});
