<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--

  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 22/07/2019
  Time: 2:17 PM
  To change this template use File | Settings | File Templates.

--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title> Thêm bản tin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

    <script type="text/javascript" src="scripts/thembantin.js"></script>

</head>
<body>
<div>
    <jsp:include page="HomeServlet"></jsp:include>
</div>
<div class="container">
    <form action="ThembantinServlet" method="post" id="form-thembantin">


        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <div class="form-group">
                    <label>Tiêu đề:</label>
                    <input type="text" class="form-control" placeholder="Nhập tiêu đề" name="tieude" id="tieude" required >
                </div>
                <div class="form-group">
                    <label>Danh mục:</label>
                    <select class="selectpicker" name="danhmuc" id="danhmuc" >
                        <c:forEach items="${listdanhmuc}" var="c">
                            <option value="${c.getIddanhmuc()}">${c.getTendanhmuc()}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                <div class="form-group">
                    <label>Tóm tắt :</label>
                    <textarea name="tomtat" id="tomtat"  cols="85" required></textarea>
                </div>
                <div class="form-group">
                    <label>Link ảnh :</label>
                    <input type="text" class="form-control"  name="linkanh" id="linkanh" required >
                </div>
            </div>

        </div>
        <div class="form-group">
            <label>Nội dung:</label>
            <textarea name="noidung" id="noidung" required ></textarea>
            <script>
                CKEDITOR.replace('noidung');
            </script>
        </div>

        <a id="btthembaiviet" class="btn btn-info"  data-toggle="modal">Thêm bài viết</a>

    </form>
    <div id="thongbaothem" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h2 class="modal-title">Thông báo thêm bài viết</h2>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning" id="canhbao">Bạn có muốn thêm bài viết</p>
                        <div id="ketqua"></div>
                        <span id="thongbaokq" style="display: none" class="alert alert-warning">Loading...</span>
                    </div>
                    <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="button" id="themngay" class="btn btn-primary" value="Thêm ngay">
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
</body>
</html>
