<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 7/15/2019
  Time: 9:18 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Các gói bảo hiểm</title>
    <link rel="stylesheet" href="styles/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/main.js"></script>
</head>
<body>

<div>
    <jsp:include page="HomeServlet"></jsp:include>
</div>
<div class="container">
    <h1>Insurance Pakage </h1>
    <table class="table table-hover" id="tbCar">
        <thead>
        <th>Tên gói bảo hiểm</th>
        <th>Biển số xe </th>
        <th>Trạng thái</th>
        <th>Loại bảo hiểm</th>
        <th>Hành động</th>
        </thead>
        <tbody>
        <c:forEach items="${listInsurance}" var="c">
                <tr class="cltr">
                <td>${c.tengoibaohiem}</td>
                <td>
                    <c:if test="${c.biensoxe==null}">
                        <span class="badge badge-danger">Chưa mua</span>
                    </c:if>
                    <c:if test="${c.biensoxe!=null}">
                        ${c.biensoxe}
                    </c:if>
                </td>
                <td>
                    <c:if test="${c.trangthaibaohiem==false}">
                        <span class="badge badge-danger">Chưa mua</span>
                    </c:if>
                    <c:if test="${c.trangthaibaohiem==true}">
                        <span class="badge badge-primary">Đã mua</span>
                    </c:if>
                </td>
                <td>${c.loaibaohien}</td>
                <td>
                    <a href="#" class="xem" data-toggle="modal"> <i class="material-icons"  data-toggle="tooltip" title="Xem">remove_red_eye</i></a>
                    <a href="#" class="sua" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Sửa">&#xE254;</i></a>
                    <a href="#" class="xoa" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Xóa">&#xE872;</i></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>


</body>
</html>
