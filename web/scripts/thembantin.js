$(document).ready(function() {

    $("#btthembaiviet").click(function () {
        $("#themngay").show();
        $("p#canhbao").show();
        $( "span#thongbaokq" ).hide();
        var messageLength = CKEDITOR.instances['noidung'].getData().replace(/<[^>]*>/gi, '').length;
        if( !messageLength ) {
            alert( 'Nhập nội dung bài viết!' );
            e.preventDefault();
        }
        if($("#form-thembantin").valid()){
            $('#thongbaothem').modal('show');
        }
    });

    $("#themngay").click(function () {
        if($("#form-thembantin").valid()){
            var $tieude=$("#tieude").val();
            var $noidung= CKEDITOR.instances['noidung'].getData();
            var $danhmuc=$("#danhmuc option:selected").val();
            var $tomtat=$("#tomtat").val();
            var $linkanh=$("#linkanh").val();
            $.post( "ThembantinServlet",{tieude:$tieude,danhmuc:$danhmuc, noidung: $noidung,tomtat:$tomtat,linkanh:$linkanh }, function( data ) {
                if(data=="true"){
                    $( "span#thongbaokq" ).text("Thêm thành công!");
                    $("#tieude").val("");
                    CKEDITOR.instances['noidung'].setData("");
                    $("#tomtat").val("");
                    $("#linkanh").val("");
                    $("#themngay").hide();
                }
                else {
                    $( "span#thongbaokq" ).text("Thêm thất bại!")
                }

            });
            $("p#canhbao").fadeOut("slow");
            $( "span#thongbaokq" ).fadeIn( "slow" );
        }



});
});
