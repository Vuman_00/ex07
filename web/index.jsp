<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: vuman_000
  Date: 7/11/2019
  Time: 11:45 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Xin chào</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="styles/home.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script src="login.js"></script>


</head>
<body>
<div>
    <jsp:include page="HomeServlet"></jsp:include>
    <c:if test="${listchitietbantin==null}">
        <c:redirect url="IndexServlet"></c:redirect>
    </c:if>
</div>

<div class="container">

    <h1>Hot News</h1>
    <div class="row clearfix">
        <c:forEach items="${listchitietbantin}" var="chitiet">
            <div class="col col-md-4 col-sm-6 col-6">
                <div class="book">
                    <div class="book_img">
                        <a href="#">
                            <img src="${chitiet.getLkanh()}" alt="Sự im lặng của bầy cừu"/>
                        </a>
                    </div>
                    <div class="book_info">
                        <h4>
                            <a href="ChitietbantinServlet?idchitiet=${chitiet. getIdchitiet()}"
                               title="${chitiet.getTieude()}">
                                <strong>${chitiet.getTieude()}</strong>
                            </a>
                        </h4>
                        <p>${chitiet.getTomtat()}</p>
<%--                    <span class="badge badge-pill badge-primary ">--%>
<%--                           <i class="material-icons" data-toggle="tooltip" title="Ngày đăng">update</i>--%>
<%--                           ${chitiet.getNgaydang()}--%>
<%--                           <i class="material-icons" data-toggle="tooltip" title="Lượt xem">visibility</i>--%>
<%--                           ${chitiet.getSolanxem()}--%>
<%--                    </span>--%>

                    </div>
                </div>
            </div>

        </c:forEach>
    </div>
    <h1>Danh sách oto chưa có bảo hiểm</h1>
    <div class="row clearfix">
        <div class="col col-md-8 ">
    <c:forEach items="${listcarnothaveinsurance}" var="c">

        <div class="book">
            <div class="book_img">
                <a href="#">
                    <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/article_image_185/public/images/car-reviews/first-drives/legacy/1-mazda-mx5-30th-anniversary-2019-fd-uk-hero-front.jpg?itok=LSThhDDQ" alt="Car Name"/>
                </a>
            </div>
            <div class="book_info">
                <h4>
                    <a href="#"
                       title="${c.getCarname()}">
                        <strong>${c.getCarname()}</strong>
                    </a>
                </h4>
                <p>Tên xe: ${c.getCarname()}<br> Biển số:${c.getBrand()}</p>
            </div>
        </div>
       </c:forEach>
            <div class="clearfix">
                <div class="hint-text">Showing <b>3</b> out of <b>25</b> entries</div>
                <ul class="pagination">
                    <li class="page-item disabled"><a href="#">Previous</a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item active"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">4</a></li>
                    <li class="page-item"><a href="#" class="page-link">5</a></li>
                    <li class="page-item"><a href="#" class="page-link">Next</a></li>
                </ul>
            </div>
        </div>

        <div class="col col-md-4 ">
            <p>Danh mục</p>
            <c:forEach items="${listdanhmuc}" var="danhmuc">
                <a href="#?iddanhmuc=${danhmuc.getIddanhmuc()}">${danhmuc.getTendanhmuc()}</a><br>
            </c:forEach>
        </div>
    </div>
</div>

</body>
</html>
